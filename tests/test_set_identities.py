# Copyright (c) 2021 Alex Jamieson-Binnie
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import AbstractSet, Iterator

import pytest

from infinite_sets import everything, complement


class UserDefinedSet(AbstractSet):

    def __init__(self, internal_set):
        self._set = set(internal_set)

    def __contains__(self, x: object) -> bool:
        return x in self._set

    def __len__(self) -> int:
        return len(self._set)

    def __iter__(self) -> Iterator:
        return self._set.__iter__()


SETS = [
    everything(),
    set(),
    {1, 2},
    complement({1, 2}),
    UserDefinedSet({1, 2})
]


@pytest.fixture(params=SETS)
def set1(request):
    return request.param


@pytest.fixture(params=SETS)
def set2(request):
    return request.param


@pytest.fixture(params=SETS)
def set3(request):
    return request.param


def test_commutativity_union(set1, set2):
    assert set1 | set2 == set2 | set1


def test_commutativity_intersection(set1, set2):
    assert set1 & set2 == set2 & set1


def test_commutativity_symmetric_difference(set1, set2):
    assert set1 ^ set2 == set2 ^ set1


def test_associativity_union(set1, set2, set3):
    assert (set1 | set2) | set3 == set1 | (set2 | set3)
    assert (set1 | set2) | set3 == set1 | set2 | set3
    assert set1 | (set2 | set3) == set1 | set2 | set3


def test_associativity_intersection(set1, set2, set3):
    assert (set1 & set2) & set3 == set1 & (set2 & set3)
    assert (set1 & set2) & set3 == set1 & set2 & set3
    assert set1 & (set2 & set3) == set1 & set2 & set3


def test_associativity_symmetric_difference(set1, set2, set3):
    assert (set1 ^ set2) ^ set3 == set1 ^ (set2 ^ set3)
    assert (set1 ^ set2) ^ set3 == set1 ^ set2 ^ set3
    assert set1 ^ (set2 ^ set3) == set1 ^ set2 ^ set3


def test_union_empty_set(set1):
    assert set1 | set() == set1


def test_intersection_empty_set(set1):
    assert set1 & set() == set()


def test_difference_empty_set(set1):
    assert set1 - set() == set1


def test_symmetric_difference_empty_set(set1):
    assert set1 ^ set() == set1


def test_absorption_union(set1, set2):
    assert set1 | (set1 & set2) == set1
    assert set1 & (set1 | set2) == set1


def test_complement_definition(set1):
    assert complement(set1) == everything() - set1


def test_demorgan_laws(set1, set2):
    assert complement(set1 | set2) == complement(set1) & complement(set2)
    assert complement(set1 & set2) == complement(set1) | complement(set2)


def test_complement_union_self(set1):
    assert set1 | complement(set1) == everything()


def test_complement_intersection_self(set1):
    assert set1 & complement(set1) == set()


def test_complement_symmetric_difference_self(set1):
    assert set1 ^ complement(set1) == everything()


def test_complement_empty_set():
    assert complement(set()) == everything()


def test_complement_universal_set():
    assert complement(everything()) == set()


def test_complement_superset(set1, set2):
    if set1 <= set2:
        assert complement(set2) <= complement(set1)


def test_complement_involution(set1):
    assert complement(complement(set1)) == set1


def test_complement_relative_complements(set1, set2):
    assert set1 - set2 == set1 & complement(set2)
    assert complement(set1 - set2) == complement(set1) | set2


def test_complement_difference_relation(set1, set2):
    assert complement(set1) - complement(set2) == set2 - set1


def test_uniqueness_of_complements(set1, set2):
    if set1 | set2 == everything() and set1 & set2 == set():
        assert set1 == complement(set2)
