# Copyright (c) 2021 Alex Jamieson-Binnie
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import AbstractSet, Iterator

import pytest

from infinite_sets import everything, complement, InfiniteSet


class UserDefinedSet(AbstractSet):

    def __init__(self, internal_set):
        self._set = set(internal_set)

    def __contains__(self, x: object) -> bool:
        return x in self._set

    def __len__(self) -> int:
        return len(self._set)

    def __iter__(self) -> Iterator:
        return self._set.__iter__()


def test_universal_contains():
    assert 1 in everything()
    assert "b" in everything()
    assert everything() in everything()
    assert set() in everything()


def test_universal_equals_self():
    assert everything() == everything()


def test_universal_not_equals_self():
    assert not (everything() != everything())


@pytest.mark.parametrize('argument', (
        set(),
        UserDefinedSet(set()),
        {1, 3},
        UserDefinedSet({1, 3}),
        complement({1, 3})
))
def test_universal_equal(argument):
    assert not (everything() == argument)
    assert not (argument == everything())


@pytest.mark.parametrize('argument', (
        set(),
        UserDefinedSet(set()),
        {1, 3},
        UserDefinedSet({1, 3}),
        complement({1, 3})
))
def test_universal_not_equal(argument):
    assert (everything() != argument)
    assert (argument != everything())


@pytest.mark.parametrize('argument', (
        everything(),
        set(),
        UserDefinedSet(set()),
        {1, 3},
        UserDefinedSet({1, 3}),
        complement({1, 3})
))
def test_universal_not_strict_subset(argument):
    assert not (everything() < argument)
    assert not (argument > everything())


def test_universal_subset_self():
    assert everything() <= everything()


@pytest.mark.parametrize('argument', (
        set(),
        UserDefinedSet(set()),
        {1, 3},
        UserDefinedSet({1, 3}),
        complement({1, 3})
))
def test_universal_not_subset(argument):
    assert not (everything() <= argument)
    assert not (argument >= everything())


def test_universal_not_strict_superset():
    assert not (everything() > everything())


@pytest.mark.parametrize('argument', (
        set(),
        UserDefinedSet(set()),
        {1, 3},
        UserDefinedSet({1, 3}),
        complement({1, 3})
))
def test_universal_strict_superset(argument):
    assert everything() > argument
    assert argument < everything()


@pytest.mark.parametrize('argument', (
        everything(),
        set(),
        UserDefinedSet(set()),
        {1, 3},
        UserDefinedSet({1, 3}),
        complement({1, 3})
))
def test_universal_superset(argument):
    assert everything() >= argument
    assert argument <= everything()


@pytest.mark.parametrize('argument', (
        everything(),
        set(),
        UserDefinedSet(set()),
        {1, 3},
        UserDefinedSet({1, 3}),
        complement({1, 3})
))
def test_universal_union(argument):
    assert (everything() | argument) == everything()
    assert (argument | everything()) == everything()


@pytest.mark.parametrize('argument', (
        everything(),
        set(),
        UserDefinedSet(set()),
        {1, 3},
        UserDefinedSet({1, 3}),
        complement({1, 3})
))
def test_universal_intersection(argument):
    assert (everything() & argument) == argument
    assert (argument & everything()) == argument


@pytest.mark.parametrize(('argument', 'result'), (
        (everything(), set()),
        (set(), everything()),
        (UserDefinedSet(set()), everything()),
        ({1, 3}, complement({1, 3})),
        (UserDefinedSet({1, 3}), complement({1, 3})),
        (complement({1, 3}), {1, 3})
))
def test_universal_difference(argument, result):
    assert everything() - argument == result
    assert argument - everything() == set()


@pytest.mark.parametrize(('argument', 'result'), (
        (everything(), set()),
        (set(), everything()),
        (UserDefinedSet(set()), everything()),
        ({1, 3}, complement({1, 3})),
        (UserDefinedSet({1, 3}), complement({1, 3})),
        (complement({1, 3}), {1, 3})
))
def test_universal_symmetric_difference(argument, result):
    assert (everything() ^ argument) == result
    assert (argument ^ everything()) == result


def test_universal_complement():
    assert complement(everything()) == set()


@pytest.mark.parametrize('argument', (
        everything(),
        set(),
        UserDefinedSet(set()),
        {1, 3},
        UserDefinedSet({1, 3}),
        complement({1, 3})
))
def test_complement_involution(argument):
    assert complement(complement(argument)) == argument


def test_complement_contains():
    assert 1 not in complement({1, 3})
    assert 2 in complement({1, 3})
    assert "b" in complement({1, 3})


def test_complement_equality():
    assert complement({1, 3}) == complement({1, 3})
    assert not (complement({1, 3}) == complement({1}))
    assert not (complement({1, 3}) == complement({1, 3, 5}))
    assert not (complement({1, 3}) == complement({1, 2}))
    assert not (complement({1, 3}) == complement({2, 4}))
    assert not (complement({1, 3}) == everything())
    assert not (complement({1, 3}) == set())
    assert not (complement({1, 3}) == {1})
    assert not (complement({1, 3}) == {1, 3})
    assert not (complement({1, 3}) == {1, 3, 5})
    assert not (complement({1, 3}) == {1, 2})
    assert not (complement({1, 3}) == {2, 4})


def test_complement_inequality():
    assert not (complement({1, 3}) != complement({1, 3}))
    assert complement({1, 3}) != complement({1})
    assert complement({1, 3}) != complement({1, 3, 5})
    assert complement({1, 3}) != complement({1, 2})
    assert complement({1, 3}) != complement({2, 4})
    assert complement({1, 3}) != everything()
    assert complement({1, 3}) != set()
    assert complement({1, 3}) != {1}
    assert complement({1, 3}) != {1, 3}
    assert complement({1, 3}) != {1, 3, 5}
    assert complement({1, 3}) != {1, 2}
    assert complement({1, 3}) != {2, 4}


@pytest.mark.parametrize(('argument', 'result'), [
    [everything(), True],
    [complement({1, 3}), False],
    [complement({1}), True],
    [complement({1, 3, 5}), False],
    [complement({1, 2}), False],
    [complement({2, 4}), False],
    [set(), False],
    [{1, 3}, False],
    [{1}, False],
    [{1, 3, 5}, False],
    [{1, 2}, False],
    [{2, 4}, False]
])
def test_complement_strict_subset(argument, result):
    assert (complement({1, 3}) < argument) == result
    assert (argument > complement({1, 3})) == result


@pytest.mark.parametrize(('argument', 'result'), [
    [everything(), True],
    [complement({1, 3}), True],
    [complement({1}), True],
    [complement({1, 3, 5}), False],
    [complement({1, 2}), False],
    [complement({2, 4}), False],
    [set(), False],
    [{1, 3}, False],
    [{1}, False],
    [{1, 3, 5}, False],
    [{1, 2}, False],
    [{2, 4}, False]
])
def test_complement_subset(argument, result):
    assert (complement({1, 3}) <= argument) == result
    assert (argument >= complement({1, 3})) == result


@pytest.mark.parametrize(('argument', 'result'), [
    [everything(), False],
    [complement({1, 3}), False],
    [complement({1}), False],
    [complement({1, 3, 5}), True],
    [complement({1, 2}), False],
    [complement({2, 4}), False],
    [set(), True],
    [{1, 3}, False],
    [{1}, False],
    [{1, 3, 5}, False],
    [{1, 2}, False],
    [{2, 4}, True]
])
def test_complement_strict_superset(argument, result):
    assert (complement({1, 3}) > argument) == result
    assert (argument < complement({1, 3})) == result


@pytest.mark.parametrize(('argument', 'result'), [
    [everything(), False],
    [complement({1, 3}), True],
    [complement({1}), False],
    [complement({1, 3, 5}), True],
    [complement({1, 2}), False],
    [complement({2, 4}), False],
    [set(), True],
    [{1, 3}, False],
    [{1}, False],
    [{1, 3, 5}, False],
    [{1, 2}, False],
    [{2, 4}, True]
])
def test_complement_superset(argument, result):
    assert (complement({1, 3}) >= argument) == result
    assert (argument <= complement({1, 3})) == result


@pytest.mark.parametrize(('argument', 'result'), [
    [everything(), everything()],
    [complement({1, 3}), complement({1, 3})],
    [complement({1}), complement({1})],
    [complement({1, 3, 5}), complement({1, 3})],
    [complement({1, 2}), complement({1})],
    [complement({2, 4}), everything()],
    [set(), complement({1, 3})],
    [{1, 3}, everything()],
    [{1}, complement({3})],
    [{1, 3, 5}, everything()],
    [{1, 2}, complement({3})],
    [{2, 4}, complement({1, 3})]
])
def test_complement_union(argument, result):
    assert (complement({1, 3}) | argument) == result
    assert (argument | complement({1, 3})) == result


@pytest.mark.parametrize(('argument', 'result'), [
    [everything(), complement({1, 3})],
    [complement({1, 3}), complement({1, 3})],
    [complement({1}), complement({1, 3})],
    [complement({1, 3, 5}), complement({1, 3, 5})],
    [complement({1, 2}), complement({1, 2, 3})],
    [complement({2, 4}), complement({1, 2, 3, 4})],
    [set(), set()],
    [{1, 3}, set()],
    [{1}, set()],
    [{1, 3, 5}, {5}],
    [{1, 2}, {2}],
    [{2, 4}, {2, 4}]
])
def test_complement_intersection(argument, result):
    assert (complement({1, 3}) & argument) == result
    assert (argument & complement({1, 3})) == result


@pytest.mark.parametrize(('argument', 'left_result', 'right_result'), [
    [everything(), set(), {1, 3}],
    [complement({1, 3}), set(), set()],
    [complement({1}), set(), {3}],
    [complement({1, 3, 5}), {5}, set()],
    [complement({1, 2}), {2}, {3}],
    [complement({2, 4}), {2, 4}, {1, 3}],
    [set(), complement({1, 3}), set()],
    [{1, 3}, complement({1, 3}), {1, 3}],
    [{1}, complement({1, 3}), {1}],
    [{1, 3, 5}, complement({1, 3, 5}), {1, 3}],
    [{1, 2}, complement({1, 2, 3}), {1}],
    [{2, 4}, complement({1, 2, 3, 4}), set()]
])
def test_complement_difference(argument, left_result, right_result):
    assert (complement({1, 3}) - argument) == left_result
    assert (argument - complement({1, 3})) == right_result


@pytest.mark.parametrize(('argument', 'result'), [
    [everything(), {1, 3}],
    [complement({1, 3}), set()],
    [complement({1}), {3}],
    [complement({1, 3, 5}), {5}],
    [complement({1, 2}), {2, 3}],
    [complement({2, 4}), {1, 2, 3, 4}],
    [set(), complement({1, 3})],
    [{1, 3}, everything()],
    [{1}, complement({3})],
    [{1, 3, 5}, complement({5})],
    [{1, 2}, complement({2, 3})],
    [{2, 4}, complement({1, 2, 3, 4})]
])
def test_complement_symmetric_difference(argument, result):
    assert (complement({1, 3}) ^ argument) == result
    assert (argument ^ complement({1, 3})) == result


def test_complement_correct():
    assert complement(everything()) == set()
    assert complement(set()) == everything()


@pytest.mark.parametrize('argument', [
    everything(),
    complement({1, 2}),
    set(),
    {1, 2}
])
def test_isinstance_infiniteset(argument):
    assert isinstance(argument, InfiniteSet)


@pytest.mark.parametrize('argument', [
    [3, 6],
    "abc",
    None,
    True
])
def test_not_isinstance_infiniteset(argument):
    assert not isinstance(argument, InfiniteSet)
