# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2021-03-30
### Changed
- Switch to package format to allow py.typed and hence typing.

## [1.0.0] - 2021-03-30
### Added
- Initial release of code.
- `everything()` method to return the universal set.
- `complement(set)` method to get the complement of a set.

[1.0.1]: https://gitlab.com/alexjbinnie/infinite-sets/-/tags/v1.0.1
[1.0.0]: https://gitlab.com/alexjbinnie/infinite-sets/-/tags/v1.0.0